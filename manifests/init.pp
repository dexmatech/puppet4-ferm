class ferm {

  $tcp = lookup( { 'name' => 'tcp', 'default_value' => undef } )
  $udp = lookup( { 'name' => 'udp', 'default_value' => undef } )
  $forwardferm = lookup( { 'name' => 'forwardferm', 'default_value' => undef } )
  $redirectferm = lookup( { 'name' => 'redirectferm', 'default_value' => undef } )
  $environment = $::facts['gce_tag_environment']


  package { 'ferm':
    ensure => installed,
  }

  file { '/etc/ferm/ferm.conf':
    ensure  => file,
    content => template('ferm/ferm.conf.erb'),
    mode    => '0644',
    owner   => 'root',
    group   => 'root',
    require => Package['ferm'],
    notify  => Service['ferm'],
  }

  service { 'ferm':
    ensure     => running,
    enable     => true,
    hasrestart => true,
    hasstatus  => false,
    require    => Package['ferm'],
  }

  file { '/etc/ferm/ferm.d/':
    ensure => absent,
  }

}
